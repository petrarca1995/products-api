/*
in RESTFUL approach, to create a resource, we use either PUT or POST

It's much clearer for consumers to know if we stick to:
1) POST for creating resource
2) PUT for modifying/updating resource


*/

package handlers

import (
	"context"
	"github.com/gorilla/mux"
	"go-youtube-series/data"
	"log"
	"net/http"
	"strconv"
)

// all we need to do to create a handler is to create a struct that implements the Handler interface
// then we can use this Handler defined by us and register it with the ServeMux

// capitalize to make it exportable

type Products struct {
	log *log.Logger
}

// constructor, log object will be injected, this allows us to easily inject other impls. In our test we can replace the log with something else.
// Injecting the logger is better than creating the logger directly in our handler class (separation of concerns, loose coupling, and testability)
func NewProduct(l *log.Logger) *Products {
	return &Products{log: l}
}

//note, httprequest body is an io reader
func (p *Products) GetProducts(w http.ResponseWriter, r *http.Request) {
	p.log.Print("GET /products")

	lp := data.GetProducts()
	err := lp.ToJson(w)
	// NOTE: data needs to be allocated by go with Marshall
	//d, err := json.Marshal(lp)
	if err != nil {
		http.Error(w, "Unable to marshal json", http.StatusInternalServerError)
	}
	//w.Write(d)
}

func (p *Products) GetProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "Error converting resource id from uri", http.StatusBadRequest)
		return
	}

	p.log.Printf("GET /product/{%d}", id)

	lp, _, err := data.GetProduct(id)

	if err == data.ErrProductNotFound {
		// bad request most likely
		http.Error(w, "ProductNotFound", http.StatusNotFound)
		return
	}

	// product found, converting to json
	err = lp.ToJson(w)

	// error converting to json, since only thing that client passes is id via path variable, at this point if there is a failure, it's son the server side
	if err != nil {
		http.Error(w, "Unable to marshal json", http.StatusInternalServerError)
		return
	}
}

// p is the struct handlers.Products, which is our products handler
func (p *Products) AddProduct(w http.ResponseWriter, r *http.Request) {
	p.log.Print("POST /products")

	// now we get the product from the request. Middleware is in charge of trying to deserialize into object, if it fails, error will be returned in the HTTP request, if it succeeds, the request will now reach this function, the handler
	prod := r.Context().Value(KeyProduct{}).(*data.Product)

	// # to see fields and values
	p.log.Printf("Prod: %#v", prod)

	// after we have a product, let's persist it
	data.AddProduct(prod)
}

func (p *Products) UpdateProduct(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "Error converting resource id from uri", http.StatusBadRequest)
		return
	}

	p.log.Printf("UPDATE /products/{%d}", id)

	// cast, this is fine because it's almost impossible for non Product type to get into context
	prod := r.Context().Value(KeyProduct{}).(*data.Product)

	// NOTE: moved this into the middleware

	//// instantiate product which will be updated
	//prod := &data.Product{}
	//
	//err = prod.FromJson(r.Body)
	//if err != nil {
	//	// bad request most likely
	//	http.Error(w, "Unable to unmarshal json", http.StatusBadRequest)
	//	return
	//}

	err = data.UpdateProduct(id, prod)

	if err == data.ErrProductNotFound {
		// bad request most likely
		http.Error(w, "ProductNotFound", http.StatusNotFound)
		return
	}

	if err != nil {
		// error is something else
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

}

type KeyProduct struct{}

// the middleware will get executed before the handler code.

// MiddlewareFunc is a function which receives an http.Handler and returns another http.Handler.
// Typically, the returned handler is a closure which does something with the http.ResponseWriter and http.Request passed
// to it, and then calls the handler passed as parameter to the MiddlewareFunc.

// Request comes in to server, gets picked up by router, router says hey, its a GET, routes it to the GET sub router, subrouter then routes to our middleware
func (p Products) MiddlewareProductValidation(next http.Handler) http.Handler {
	// HandlerFunc type is an adapter that allows the use of ordinary functions as HTTP handlers. If f is a function with the appropriate sig, HandlerFunc(f) is a Handler that calls f
	// HandlerFunc is a Handler because it implements the ServeHTTP(ResponseWriter, *Request) interface
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {

		prod := &data.Product{}

		err := prod.FromJson(req.Body)
		if err != nil {
			// bad request most likely
			http.Error(w, "Unable to unmarshal json", http.StatusBadRequest)
			return
		}

		// creating a new context from the context of the request and adding product to it
		//TODO figure out what can be passed as key, here is seems we pass an instance of type KeyProduct
		ctx := context.WithValue(req.Context(), KeyProduct{}, prod)
		// creating the new request (shallow copy) with its context changed to the one above
		reqWithProd := req.WithContext(ctx)

		// calls the handler that was passed to it as the parameter. This is the next one to be executed in the chain, so this could be more middleware or the final, actual handler
		// the order in which Use called with middleware will be the order of middleware chaining
		next.ServeHTTP(w, reqWithProd)
	})
}

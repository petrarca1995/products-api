package main

import (
	"context"
	"github.com/gorilla/mux"
	"go-youtube-series/handlers"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {

	// creating logger, output will be StdOut
	l := log.New(os.Stdout, "product-api", log.LstdFlags)
	ph := handlers.NewProduct(l)

	// a router has concepts of sub router, we can follow a pattern where we define a sub Router for each method which allows us to be more flexible with adding  middleware
	sm := mux.NewRouter()

	// Methods("GET") returns a *Route, the method SubRouter() converts it into a *Router, the same as sm, and once it's a router we can add a handler to it
	getRouter := sm.Methods(http.MethodGet).Subrouter()
	getRouter.HandleFunc("/products", ph.GetProducts)
	getRouter.HandleFunc("/products/{id:[0-9]+}", ph.GetProduct)
	putRouter := sm.Methods(http.MethodPut).Subrouter()
	// curly brackets is how we define regex
	// when we define our path variables in our uri, Gorilla will extract them into mux.vars
	putRouter.HandleFunc("/products/{id:[0-9]+}", ph.UpdateProduct)
	putRouter.Use(ph.MiddlewareProductValidation)
	// creating post sub router, which is also a router
	postRouter := sm.Methods(http.MethodPost).Subrouter()
	postRouter.HandleFunc("/products", ph.AddProduct)
	postRouter.Use(ph.MiddlewareProductValidation)

	// constructs an http server and registers a Handler to it
	// the second parameter is a handler, if we pass null the DefaultServeMux will be used
	//http.ListenAndServe(":8080", sm)

	// we need to start thinking of tuning the server parameters. Time out is an important one because, if a client connects, and time out is too long, we are using up that connection for a while and our server only has limited resources. If we have multiple connections that are blocked, our server can end up failing to serve requests.
	//important parameters:
	// 1) Idle Timeout:
	//   - all around connection pooling, when a connection is made ot the server, doing the DNS lookup and handshake is not free. Making the connection takes time.
	//	 - so if we have a lot of requests from a single client, we can make it more efficient by keeping the connection open. The client will use the same open connection over and over. This is useful in microservices architecture.
	//   - if requests comes from random clients all of the time (end users) we generally could run a low idle timeout
	s := &http.Server{
		Addr:         ":9090",
		Handler:      sm,
		IdleTimeout:  120 * time.Second,
		ReadTimeout:  1 * time.Second,
		WriteTimeout: 1 * time.Second,
	}

	//graceful shutdown:
	// - If we have a client doing a large file upload, or a DB transaction, and I decide to shut server down to upgrade the version, etc and If we don't do it gracefully, we run risk of disconnecting client midway, generating an error
	// - The above is not ideal, with the Go server, we can use a shutdown() function
	// with this function, server will stop receiving requests, it will finish the current requests and then shutdown

	// the process of graceful shutdown is as follows:
	// 1) listen for OS signals, and
	// 2) handle those signals

	// firing a new go routine because s.ListenAndServe() blocks
	go func() {
		err := s.ListenAndServe()
		if err != nil {
			l.Fatal(err)
		}

	}()

	// os signal package is used to interact with OS signals, which are messages sent to a running program to trigger a specific behavior, such as quitting. They are a form of IPC (Inter Process Communication). Ctrl - C sends an INT signal ("interrupt", SIGINT); by default this cases the process to terminate.
	// making a channel. These are communication channels between go routines
	// they are used when you want to pass in results or errors or any other kind of information from one goroutine to another
	// channels have types, the type is the type of information that we want to receive
	// in this case, we want to receive os.Signal type
	// https://en.wikipedia.org/wiki/Signal_(IPC)
	// https://gobyexample.com/signals
	sigChan := make(chan os.Signal)
	// broadcasts a message on this channel when either an os kill or os interrupt is received
	signal.Notify(sigChan, os.Interrupt)
	signal.Notify(sigChan, os.Kill)

	// this blocks. Reading from a channel will block until there is a message available to be consumed
	sig := <-sigChan
	l.Println("Received terminate, graceful shutdown", sig)

	// what this does is that, if we shutdown server, the Server won't kill all connections to it, it will instead, finish the requests without accepting new ones and then shutdown
	// it is a graceful shutdown
	// We will allow 30 seconds for all connections to finish

	// this method takes the parent context, context.Background()  returns a non-nil, empty context
	// https://stackoverflow.com/questions/64327009/context-withdeadline-vs-withtimeout
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	s.Shutdown(ctx)

}

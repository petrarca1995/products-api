package data

import (
	"encoding/json"
	"fmt"
	"io"
	"time"
)

type Product struct {
	ID          int     `json:"id"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Price       float32 `json:"price"`
	SKU         string  `json:"sku"`
	CreatedOn   string  `json:"-"`
	UpdatedOn   string  `json:"-"`
	DeletedOn   string  `json:"-"`
}

// doing this allows us to add a method
// again, we like to encapsulate logic here in case we want to use a different decoder or we want some logic before decoding, in order to prevent affecting the handler method
func (p *Product) FromJson(r io.Reader) error {
	decoder := json.NewDecoder(r)
	// decode will copy the structure of the request into the structure passed as argument, we can pass ourselves here because we have a pointer. So this essentially moves the data from the reader (request.body) to the products
	return decoder.Decode(p) // decoder returns an error so we can just return that
}

func (p *Product) ToJson(w io.Writer) error {
	encoder := json.NewEncoder(w)
	// decode will copy the structure of the request into the structure passed as argument, we can pass ourselves here because we have a pointer. So this essentially moves the data from the reader (request.body) to the products
	return encoder.Encode(p) // decoder returns an error so we can just return that
}

// doing this allows us to add a method. https://www.youtube.com/watch?v=eBeqtmrvVpg&ab_channel=NicJackson min 21:45
type Products []*Product

//NOTE: NIC likes to ass the encoding/decoding logic into its data structure

// encapsulating logic of converting to json in the method of Products, if we wanted to add some manipulation, we could do so cleanly encapsulated in this ToJson method belonging to the Products struct
func (p *Products) ToJson(w io.Writer) error {
	encoder := json.NewEncoder(w)
	return encoder.Encode(p)
}

// it is useful to abstract the detail logic of where the data is coming from, from the rest of the code. Best to add this abstraction to our data access model

//TODO this is not a method because it does not belong to our struct that is a list of products
func GetProducts() Products {
	// TODO productList is a slice of pointer to Product. How does this compile when method signature returns Products
	return productList
}

func GetProduct(id int) (*Product, int, error) {
	product, pos, err := findProduct(id)
	if err != nil {
		return nil, -1, err
	}
	return product, pos, nil
}

func AddProduct(p *Product) {
	// id wont be sent by client, it's our responsibility. This will actually be done in the DB
	p.ID = getNextID()
	productList = append(productList, p)
}

func UpdateProduct(id int, p *Product) error {
	// find product
	_, pos, err := findProduct(id)
	// if no product present
	if err != nil {
		return err
	}

	p.ID = id
	productList[pos] = p
	// no errors
	return nil
}

var ErrProductNotFound = fmt.Errorf("Product Not Found")

func findProduct(id int) (*Product, int, error) {
	// TODO this is for play purpose, logic of auto increment will be moved to DB layer

	for i, p := range productList {
		if p.ID == id {
			return p, i, nil
		}
	}

	return nil, 0, ErrProductNotFound
}

// TODO this is for play purpose, logic of auto increment will be moved to DB layer
func getNextID() int {
	lp := productList[len(productList)-1]
	return lp.ID + 1
}

var productList = []*Product{
	&Product{
		ID:          1,
		Name:        "Latte",
		Description: "Frothy milk",
		Price:       2.45,
		SKU:         "Abc",
		CreatedOn:   time.Now().UTC().String(),
		UpdatedOn:   time.Now().UTC().String(),
	},
	&Product{
		ID:          2,
		Name:        "Espresso",
		Description: "String coffee",
		Price:       3.45,
		SKU:         "fjk",
		CreatedOn:   time.Now().UTC().String(),
		UpdatedOn:   time.Now().UTC().String(),
	},
}
